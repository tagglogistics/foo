from django.contrib import admin
from django.urls import path
from foo.nasa import views as nasa_views


urlpatterns = [
    path('admin/', admin.site.urls),

    path('', nasa_views.homepage),
]
